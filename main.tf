resource "helm_release" "metallb" {
  name             = "metallb"
  repository       = "https://metallb.github.io/metallb"
  chart            = "metallb"
  version          = var.metallb_version
  namespace        = var.metallb_namespace
  timeout          = var.timeout
  create_namespace = var.create_namespace

  values = [
    file(var.metallb_values)
  ]
}
resource "kubectl_manifest" "metallb_ip_address_pool" {
  yaml_body = <<-EOF
    apiVersion: metallb.io/v1beta1
    kind: IPAddressPool
    metadata:
      name: first-pool
      namespace: ${var.metallb_namespace}
    spec:
      addresses:
      - ${var.metallb_address_pool}
      avoidBuggyIPs: true
    EOF

  depends_on = [helm_release.metallb]
}
resource "kubectl_manifest" "metallb_l2_advertisement" {
  yaml_body = <<-EOF
    apiVersion: metallb.io/v1beta1
    kind: L2Advertisement
    metadata:
      name: metallbl2advertisement
      namespace: ${var.metallb_namespace}
    EOF

  depends_on = [helm_release.metallb]
}
resource "helm_release" "nginx_ingress_controller" {
  name             = "nginx-ingress"
  chart            = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  version          = var.nginx_version
  namespace        = var.nginx_namespace
  timeout          = var.timeout
  create_namespace = var.create_namespace

  values = [
    file(var.nginx_helm_values)
  ]

  depends_on = [helm_release.metallb]
}
