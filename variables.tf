variable "metallb_version" {
  description = "The version of metallb to install"
  type        = string
  default     = "0.13.11"
}
variable "metallb_namespace" {
  description = "The namespace to install metallb into"
  type        = string
  default     = "metallb-system"
}
variable "metallb_address_pool" {
  description = "The address pool (cidr) to use for metallb for example 192.168.1.0/24"
  type        = string
}
variable "metallb_values" {
  description = "The helm values to use for metallb"
  type        = string
}
variable "nginx_version" {
  description = "The version of nginx to install"
  type        = string
  default     = "4.4.2"
}
variable "nginx_namespace" {
  description = "The namespace to install nginx into"
  type        = string
  default     = "nginx-ingress"
}
variable "nginx_helm_values" {
  description = "The helm values to use for nginx"
  type        = string
}
variable "timeout" {
  description = "Timeout in seconds"
  type        = number
  default     = 600
}
variable "create_namespace" {
  description = "Create the namespace"
  type        = bool
  default     = true
}
